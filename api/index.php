
<?php
error_reporting(E_ERROR);
/**
 * Created by PhpStorm.
 * User: Allisson Mateus
 * Date: 16/05/2016
 * Time: 20:56
 */

	require 'slim/slim.php';
	

	$app = new Slim();
	$app->response()->header('Content-Type', 'application/json;charset=utf-8');
	error_reporting(E_ALL & ~E_NOTICE);
	
	
	$app->get("/teste" , function(){
		echo json_encode(
			array('Retorno'=>true)
		);
	});
	
	$app->get("/memoria" , function(){
		$tip_mem = array("MEMÓRIA", "SWAP");
		$memoria = array();
		foreach ($tip_mem as $mem) {
			
			if($mem == "MEMÓRIA") {
				$memoria['mem_total'] = `free -m | grep "Mem:" | awk -F" " '{print $2}'`;
				$memoria['buffers'] = `free -m | grep "Mem:" | awk -F" " '{print $6}'`;
				$memoria['cache'] = `free -m | grep "Mem:" | awk -F" " '{print $7}'`;
				$memoria['free'] = `free -m | grep "Mem:" | awk -F" " '{print $4}'`;
				$memoria['mem_used'] = $memoria['mem_total'] - ($memoria['cache'] + $memoria['buffers'] + $memoria['free']);
				$memoria['qtd_mem_used'] = $memoria['mem_used'] / $memoria['mem_total'] * 100;
				$memoria['valor_mem'] = sprintf("%.2f%%", $memoria['qtd_mem_used']);
			}
			elseif($mem == "SWAP") {
				$memoria['mem_total_swap'] = `free -m | grep "Swap" | awk -F" " '{print $2}'`;
				$memoria['mem_used_swap'] = `free -m | grep "Swap" | awk -F" " '{print $3}'`;
				$memoria['qtd_mem_used_swap'] = $memoria['mem_used_swap'] / $memoria['mem_total_swap'] * 100;
				$memoria['valor_mem_swap'] = sprintf("%.2f%%", $memoria['qtd_mem_used_swap']);
			}	
		
		}
		echo json_encode(
			$memoria
		);
	});
	
	$app->get("/cpu" , function(){
		$process = array("cpu00", "cpu01", "total", "Cpu");
		$cpu_ = array();
		foreach ($process as $processador) {
			if($processador == "Cpu") {
				$cpu = `top -bn2 | grep $processador | tail -1 | awk -F"," '{ print $4 }' | awk -F"%" '{ print $1 }'`;
			}
			else {
				$cpu = `top -bn0 | grep $processador | awk -F" " '{ print $8 }' | awk -F"," '{ print $1"."$2 }'`;
			}
			//echo $cpu  . "<br>";
			if(!empty($cpu)) {
				$cpu_['valor_cpu'] = 100 - $cpu;
			}
		}
		echo json_encode(
			$cpu_
		);
	});
	
	
	$app->get("/programas" , function(){
		$prog_nota = array("sasou39", "sasou43", "sasou25", "svdou25", "scdou27", "sasou46", "sasou47", "flexjava");
		$programas = array();
		foreach ($prog_nota as $prog) {
			array_push( $programas  ,   array('programa' => $prog , 'ver_nota' => `ps -ef | grep -v grep | grep $prog`, 'usuario' => `ps -ef | grep $prog | grep -v grep | awk -F" " '{ print $1 }'`  )   );
		}
		echo json_encode(
			$programas
		);
	});
	
	
	$app->get("/impressoras" , function(){
		$ips_imp = array( "cpd"=>"172.20.83.233" , "gerência(ger)"=>"172.20.83.232" , "recebimento(rme)"=>"172.20.83.229" , "recursos humanos(rhu)"=>"172.20.83.230", "caixa empresa(nfe)"=>"172.20.83.236");
		$impressoras = array();
		foreach( $ips_imp as $key => $value){
			$pingando = `ping -c 1 $value | grep "transmitted" | cut -c1-25`;
			$r = explode(' ' , $pingando);
			if(  $r[3] == '1' )$isOn = true;
			else $isOn = false;
			array_push( $impressoras, array('impressora'=>$key,'ip'=>$value,'isOn'=>$isOn)  );
		}
		echo json_encode(
			$impressoras
		);
	});
	
	
	$app->get("/usuarios" , function(){
		$usuarios = array();
		$lista=`who -u > users.txt`;
		$lista1=`cat users.txt | awk '{print $1}' > users1.txt`;
		$insere2=`sed "s/$/,/g" users1.txt > users01.txt`;
		$lista3=`cat users01.txt`;


		$users = explode(",",$lista3);
	
		$count = 0;
		$total = 0;
		$aux2 = array();
		foreach($users as $user){
			if( strlen($user) > 1 ){
				array_push( $aux2 ,  $user );
				$count ++;
				
				if($count == 4){
					array_push($usuarios ,$aux2 );
					$aux2 = array();
					$count = 0;
				}
				
				$total ++;
			}
		}
		if(count($aux2) > 0)
			array_push($usuarios, $aux2);
		echo json_encode(
			array( 'qtdOnline' => $total , 'list' => $usuarios )
 		);
	});
	
	
	$app->get("/files" , function(){
		$filesystem = array(
		"/" => "90",
		"/boot" => "90",
		"/fs0" => "85",
		"/fs1" => "90",
		"/fs1/pdv" => "90",
		"/fs2" => "90",
		"/files" => "85",
		"/opt/CA" => "85"
		);
		$system= array();
		foreach ($filesystem as $file => $limite) {
			$prc1 = `df -h | cut -c20-60 | grep -v LV | awk -F" " '{print $5";"$2";"$4}' | grep -v Mon | grep "$file;" | awk -F";" '{ print $3 }' | awk -F"%" '{print $1}'`;
			$prc = str_pad(rtrim(ltrim($prc1)), 2, "0", STR_PAD_LEFT);
			$tamanho = `df -h | cut -c20-60 | grep -v LV | awk -F" " '{print $5";"$2";"$4}' | grep -v Mon | grep "$file;" | awk -F";" '{ print $2 }'`;
			if($tamanho)
				array_push( $system , array('dir'=>$file,'tam_total'=>$tamanho,'porc'=>$prc) );
		}
		echo json_encode(
			$system
		);
	});	
	
	$app->run();
?>

